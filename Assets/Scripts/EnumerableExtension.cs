﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class EnumerableExtension
{
    public static IEnumerable<float> CumulativeSum(this IEnumerable<float> numbers)
    {
        float summedNumber = 0;
        foreach (float number in numbers)
        {
            summedNumber = summedNumber + number;
            yield return summedNumber;
        }
    }
}

