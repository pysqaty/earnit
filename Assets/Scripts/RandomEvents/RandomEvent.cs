﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RandomEvent : ScriptableObject
{
    public string Title;
    [Range(0.0f, 100.0f)]
    public float Probability;
    public string Description;
    public List<RandomEventOption> Options;
}
