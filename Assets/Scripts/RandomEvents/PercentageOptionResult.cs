﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu]
public class PercentageOptionResult : OptionResult
{
    [SerializeField]
    private float Percentage;

    public override void OperateResult()
    {
        OperationalVariable.RuntimeValue *= (Percentage / 100.0f);
    }
}

