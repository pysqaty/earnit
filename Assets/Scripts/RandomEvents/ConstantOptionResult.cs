﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu]
public class ConstantOptionResult : OptionResult
{
    [SerializeField]
    private float Effect;

    public override void OperateResult()
    {
        OperationalVariable.RuntimeValue += Effect;
    }
}

