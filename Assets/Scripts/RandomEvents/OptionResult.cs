﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public abstract class OptionResult : ScriptableObject
{
    public string Message;
    [Range(0.0f, 100.0f)]
    public float Probability;
    [SerializeField]
    protected ImmutableFloatVariable OperationalVariable;
    public abstract void OperateResult();
}