﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager
{
    private float time;
    public TimeManager()
    {
        time = 0;
    }
    public bool HasTimePassed(float deltaTime, float timeToPass)
    {
        time += deltaTime;
        if(time >= timeToPass)
        {
            time = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public IEnumerator DelayedAction(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public string GetTimeString(float time)
    {
        int hours = (int)time / 3600;
        int remainder = (int)time - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        int milisecs = (int)((time - (int)time) * 100);
        return IntToString(hours) + ":" + IntToString(mins) + ":" + IntToString(secs) + "." + IntToString(milisecs);
    }

    private string IntToString(int val)
    {
        return val < 10 ? "0" + val : "" + val;
    }
}