﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Gameplay variables and rules")]
    [SerializeField]
    private ImmutableFloatVariable currentMoney;
    [SerializeField]
    private ImmutableFloatVariable currentTime;
    [SerializeField]
    private FloatVariable currentHighscore;
    [SerializeField]
    private ImmutableFloatVariable moneyToWin;
    [SerializeField]
    private MainGameVariables gameRules;
    [Space]
    [Header("Events")]
    [SerializeField]
    private GameEvent moneyEarned;
    [SerializeField]
    private GameEvent openCloseShopEvent;
    [SerializeField]
    private GameEvent bonusAvailabilityEvent;
    [SerializeField]
    private GameEvent clearGameData;
    [SerializeField]
    private GameEvent gameFinished;

    private TimeManager timeManager;

    private void OnEnable()
    {
        Time.timeScale = 1;
        timeManager = new TimeManager();
        bonusAvailabilityEvent.Raise();
    }

    private void Update()
    {
        currentTime.RuntimeValue += Time.deltaTime;
        if(timeManager.HasTimePassed(Time.deltaTime, gameRules.PassiveIncomeInterval.RuntimeValue * gameRules.PassiveIncomeIntervalMultiplier.RuntimeValue))
        {
            UpdateCurrentMoney(gameRules.PassiveIncome.RuntimeValue * gameRules.PassiveIncomeMultiplier.RuntimeValue);
        }
    }

    public void TapToEarnClicked()
    {
        UpdateCurrentMoney(gameRules.ActiveIncome.RuntimeValue * gameRules.ActiveIncomeMultiplier.RuntimeValue);
    }

    private void UpdateCurrentMoney(float money)
    {
        currentMoney.RuntimeValue += money;
        moneyEarned.Raise();
        CheckWinCondition();
    }

    public void CheckWinCondition()
    {
        if(currentMoney.RuntimeValue >= moneyToWin.RuntimeValue)
        {
            if(currentHighscore.Value < 0 ||
                currentTime.RuntimeValue < currentHighscore.Value)
            {
                currentHighscore.Value = currentTime.RuntimeValue;
            }
            Time.timeScale = 0;
            gameFinished.Raise();
        }
    }


    public void BackToMenu()
    {
        clearGameData.Raise();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void OpenShop()
    {
        timeManager.PauseGame();
        openCloseShopEvent.Raise();
    }

    public void CloseShop()
    {
        timeManager.ResumeGame();
        bonusAvailabilityEvent.Raise();
        openCloseShopEvent.Raise();
    }
}
