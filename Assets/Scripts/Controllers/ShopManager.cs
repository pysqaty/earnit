﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    [Serializable]
    private class Button2BonusConnection
    {
        public Button button;
        public Bonus bonus;
    }
    [SerializeField]
    private List<Button2BonusConnection> buttonsWithBonuses;

    private void OnEnable()
    {
        foreach (Button2BonusConnection connection in buttonsWithBonuses)
        {
            connection.button.onClick.AddListener(() => {
                if(CanBuyBonus(connection.bonus))
                {
                    connection.bonus.Amount++;
                }
            });
        }
    }

    private bool CanBuyBonus(Bonus bonus)
    {
        return PaymentPlatformConnection.Connection.GetAccountFunds() > bonus.RealMoneyCost;
    }
}
