﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{
    [SerializeField]
    private ImmutableFloatVariable money;
    [SerializeField]
    private ImmutableFloatVariable time;
    [SerializeField]
    private FloatVariable highscore;
    [SerializeField]
    private List<ImmutableFloatVariable> gameplayVariables;
    [SerializeField]
    private List<Upgrade> upgrades;
    [SerializeField]
    private List<Bonus> bonuses;
    public void Clear()
    {
        foreach (ImmutableFloatVariable val in gameplayVariables)
        {
            val.Reset();
        }
        time.Reset();
        money.Reset();
        foreach(Upgrade up in upgrades)
        {
            up.Reset();
        }
    }

    public void SaveData()
    {
        foreach(Bonus bonus in bonuses)
        {
            PlayerPrefs.SetInt(bonus.name, bonus.Amount);
        }
    }

    public void SaveHighScore()
    {
        PlayerPrefs.SetFloat(highscore.name, highscore.Value);
    }
}
