﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomEventManager : MonoBehaviour
{
    [Header("Main gameplay fields")]
    [SerializeField]
    private float TimeBetweenPossibleRandomEvents;
    [SerializeField]
    private List<RandomEvent> possibleRandomEvents;
    [Space]
    [Header("Events")]
    [SerializeField]
    private GameEvent randomEventOccured;
    [SerializeField]
    private GameEvent moneyEarned;
    [SerializeField]
    private GameEvent checkWinCondition;
    [Space]
    [Header("UI")]
    [SerializeField]
    private TMPro.TextMeshProUGUI title;
    [SerializeField]
    private TMPro.TextMeshProUGUI description;
    [SerializeField]
    private TMPro.TMP_Dropdown options;
    [SerializeField]
    private TMPro.TextMeshProUGUI resultMessage;
    [SerializeField]
    private Button confirmButton;
    [SerializeField]
    private Button tapToContinueButton;

    private CanvasVisibility objectVisibility;
    private TimeManager timeManager;
    private RandomEvent randomEvent;
    private RandomEventOption chosenOption;
    private List<float> randomEventsThresholds;

    private void OnEnable()
    {
        timeManager = new TimeManager();
        objectVisibility = GetComponent<CanvasVisibility>();
        randomEventsThresholds = possibleRandomEvents.Select(r => r.Probability).CumulativeSum().ToList();
    }

    private void Update()
    {
        if (timeManager.HasTimePassed(Time.deltaTime, TimeBetweenPossibleRandomEvents))
        {
            CheckRandomEvent();
        }
    }

    private void CheckRandomEvent()
    {
        float randomValue = UnityEngine.Random.Range(0f, 100f);
        for (int i = 0; i < randomEventsThresholds.Count; i++)
        {
            if (randomValue < randomEventsThresholds[i])
            {
                randomEvent = possibleRandomEvents[i];
                chosenOption = randomEvent.Options[0];
                ShowRandomEvent();
                break;
            }
        }
    }

    public void ShowRandomEvent()
    {
        resultMessage.text = "";
        tapToContinueButton.interactable = false;
        confirmButton.interactable = true;
        title.text = randomEvent.Title;
        description.text = randomEvent.Description;
        options.options.Clear();
        for (int i = 0; i < randomEvent.Options.Count; i++)
        {
            RandomEventOption randomEventOption = randomEvent.Options[i];
            options.options.Add(new TMPro.TMP_Dropdown.OptionData(randomEventOption.Name));
        }
        Time.timeScale = 0;
        options.value = 0;
        options.RefreshShownValue();
        randomEventOccured.Raise();
        objectVisibility.ChangeVisibility();
    }

    public void ChooseOption(int optionIndex)
    {
        chosenOption = randomEvent.Options[optionIndex];
    }

    public void ConfirmOption()
    {
        float randomValue = UnityEngine.Random.Range(0f, 100f);
        List<float> thresholds = chosenOption.Results.Select(r => r.Probability).CumulativeSum().ToList();
        for(int i = 0; i < thresholds.Count; i++)
        {
            if(randomValue < thresholds[i])
            {
                confirmButton.interactable = false;
                tapToContinueButton.interactable = true;
                resultMessage.text = chosenOption.Results[i].Message;
                chosenOption.Results[i].OperateResult();
                break;
            }
        }
    }
    public void TapToContinueButtonClicked()
    {
        moneyEarned.Raise();
        Time.timeScale = 1;
        randomEventOccured.Raise();
        objectVisibility.ChangeVisibility();
        checkWinCondition.Raise();
    }
}