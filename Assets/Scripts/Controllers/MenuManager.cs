﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private FloatVariable highscore;
    [SerializeField]
    private TMPro.TextMeshProUGUI highscoreLabel;
    private TimeManager timeManager;

    public void OnEnable()
    {
        timeManager = new TimeManager();
        highscoreLabel.text = highscore.Value < 0 ? "None" : timeManager.GetTimeString(highscore.Value);
    }

    public void StartGameClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ExitGameClick()
    {
        Application.Quit();
    }
}
