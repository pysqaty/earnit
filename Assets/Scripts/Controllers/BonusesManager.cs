﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusesManager : MonoBehaviour
{
    [SerializeField]
    private GameEvent bonusAvailabilityEvent;

    [Serializable]
    private class Button2BonusConnection
    {
        public Button button;
        public Bonus bonus;
    }
    [SerializeField]
    private List<Button2BonusConnection> buttonsWithBonuses;


    private void OnEnable()
    {
        foreach (Button2BonusConnection connection in buttonsWithBonuses)
        {
            connection.button.onClick.AddListener(() => StartCoroutine(TurnOnBonus(connection.bonus)));
            connection.button.GetComponent<GameEventListener>().Response.AddListener(() => connection.button.interactable = (!connection.bonus.IsRunning && connection.bonus.Amount > 0));
        }
    }

    private IEnumerator TurnOnBonus(Bonus bonus)
    {
        bonus.Amount--;
        bonus.IsRunning = true;
        bonusAvailabilityEvent.Raise();
        bonus.StartBonus();
        yield return new WaitForSeconds(bonus.Duration);
        bonus.EndBonus();
        bonus.IsRunning = false;
        bonusAvailabilityEvent.Raise();
    }
}
