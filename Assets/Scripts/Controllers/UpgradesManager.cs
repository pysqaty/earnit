﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UpgradesManager : MonoBehaviour
{
    [SerializeField]
    private ImmutableFloatVariable currentMoney;
    [SerializeField]
    private GameEvent moneyChanged;

    [Serializable]
    private class Button2UpgradeConnection
    {
        public Button button;
        public Upgrade upgrade;
    }
    [SerializeField]
    private List<Button2UpgradeConnection> buttonsWithUpgrades;

    private void OnEnable()
    {
        foreach(Button2UpgradeConnection connection in buttonsWithUpgrades)
        {
            connection.button.onClick.AddListener(() => UpgradeButtonClick(connection.upgrade));
            connection.button.GetComponent<GameEventListener>().Response.AddListener(() => connection.button.interactable = IsUpgradeAvailable(connection.upgrade));
        }
    }

    private void UpgradeButtonClick(Upgrade up)
    {
        up.UpdateValue(currentMoney);
        moneyChanged.Raise();
    }

    private bool IsUpgradeAvailable(Upgrade up)
    {
        return up.IsAvailable(currentMoney.RuntimeValue);
    }
}
