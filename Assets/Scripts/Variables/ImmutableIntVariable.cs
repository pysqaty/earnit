﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ImmutableIntVariable : ScriptableObject
{
    [SerializeField]
    private int Value;

    [NonSerialized]
    public int RuntimeValue;

    private void OnEnable()
    {
        Reset();
    }

    public void Reset()
    {
        RuntimeValue = Value;
    }
}
