﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
    public float Value;

    private void OnEnable()
    {
        if(PlayerPrefs.HasKey(name))
        {
            Value = PlayerPrefs.GetFloat(name);
        }
    }
}
