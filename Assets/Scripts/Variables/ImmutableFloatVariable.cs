﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ImmutableFloatVariable : ScriptableObject
{
    [SerializeField]
    private float Value;

    [NonSerialized]
    public float RuntimeValue;

    private void OnEnable()
    {
        Reset();
    }

    public void Reset()
    {
        RuntimeValue = Value;
    }
}