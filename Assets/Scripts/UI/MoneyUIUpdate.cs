﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyUIUpdate : MonoBehaviour
{
    [SerializeField]
    private ImmutableFloatVariable currentMoney;
    private TMPro.TextMeshProUGUI currentMoneyUI;

    private void OnEnable()
    {
        currentMoneyUI = GetComponent<TMPro.TextMeshProUGUI>();
        UpdateMoney();
    }

    public void UpdateMoney()
    {
        currentMoneyUI.text = currentMoney.RuntimeValue.ToString("N2");
    }
}
