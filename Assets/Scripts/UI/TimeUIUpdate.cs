﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeUIUpdate : MonoBehaviour
{
    [SerializeField]
    private ImmutableFloatVariable currentTime;
    private TMPro.TextMeshProUGUI currentTimeUI;
    private TimeManager timeManager;

    private void OnEnable()
    {
        timeManager = new TimeManager();
        currentTimeUI = GetComponent<TMPro.TextMeshProUGUI>();
        currentTimeUI.text = timeManager.GetTimeString(currentTime.RuntimeValue);
    }

    private void Update()
    {
        currentTimeUI.text = timeManager.GetTimeString(currentTime.RuntimeValue);
    }
}