﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasVisibility : MonoBehaviour
{
    private Canvas Canvas;

    void Start()
    {
        Canvas = GetComponent<Canvas>();
    }
    public void ChangeVisibility()
    {
        if(Canvas != null)
        {
            Canvas.enabled = !Canvas.enabled;
        }
    }
}
