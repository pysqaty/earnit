﻿using UnityEngine;

[CreateAssetMenu]
public class MainGameVariables : ScriptableObject
{
    public ImmutableFloatVariable PassiveIncome;
    public ImmutableFloatVariable PassiveIncomeInterval;
    public ImmutableFloatVariable ActiveIncome;
    public ImmutableFloatVariable PassiveIncomeMultiplier;
    public ImmutableFloatVariable PassiveIncomeIntervalMultiplier;
    public ImmutableFloatVariable ActiveIncomeMultiplier;
}
