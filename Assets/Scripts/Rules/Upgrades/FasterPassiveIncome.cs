﻿using UnityEngine;

[CreateAssetMenu]
public class FasterPassiveIncome : Upgrade
{
    [SerializeField]
    private float UpgradeLimit1;
    [SerializeField]
    private float UpgradeLimit2;
    [SerializeField]
    private float TimeDifference1;
    [SerializeField]
    private float TimeDifference2;
    [SerializeField]
    private float CostDifference1;
    [SerializeField]
    private float CostDifference2;
    [SerializeField]
    private float LimitToChangeCostDifference;
    [SerializeField]
    private float FirstCost;
    [SerializeField]
    private ImmutableFloatVariable PassiveIncomeInterval;

    private void OnEnable()
    {
        Reset();
    }

    public override bool IsAvailable(float money)
    {
        return (money >= NextCost &&
            PassiveIncomeInterval.RuntimeValue - TimeDifference2 >= UpgradeLimit2);
    }

    public override void Reset()
    {
        NextCost = FirstCost;
    }

    public override void UpdateValue(ImmutableFloatVariable money)
    {
        money.RuntimeValue -= NextCost;
        if (PassiveIncomeInterval.RuntimeValue > UpgradeLimit1)
        {
            PassiveIncomeInterval.RuntimeValue -= TimeDifference1;
        }
        else
        {
            PassiveIncomeInterval.RuntimeValue -= TimeDifference2;
        }
        UpdateNextCost();
    }

    private void UpdateNextCost()
    {
        if (NextCost < LimitToChangeCostDifference)
        {
            NextCost += CostDifference1;
        }
        else
        {
            NextCost += CostDifference2;
        }
    }
}

