﻿using UnityEngine;

public abstract class Upgrade : ScriptableObject
{
    public float NextCost { get; protected set; }
    public abstract void UpdateValue(ImmutableFloatVariable money);
    public abstract bool IsAvailable(float money);
    public abstract void Reset();
}
