﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class MorePassiveIncome : Upgrade
{
    [SerializeField]
    private float UpgradeLimit;
    [SerializeField]
    private float IncomeDifference;
    [SerializeField]
    private float CostDifference;
    [SerializeField]
    private float FirstCost;
    [SerializeField]
    private ImmutableFloatVariable PassiveIncome;

    public override bool IsAvailable(float money)
    {
        return (money >= NextCost &&
            PassiveIncome.RuntimeValue + IncomeDifference <= UpgradeLimit);
    }

    private void OnEnable()
    {
        Reset();
    }

    public override void Reset()
    {
        NextCost = FirstCost;
    }

    public override void UpdateValue(ImmutableFloatVariable money)
    {
        money.RuntimeValue -= NextCost;
        PassiveIncome.RuntimeValue += IncomeDifference;
        UpdateNextCost();
    }

    private void UpdateNextCost()
    {
        NextCost += CostDifference;
    }
}