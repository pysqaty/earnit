﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class MoreActiveIncome : Upgrade
{
    [SerializeField]
    private float UpgradeLimit;
    [SerializeField]
    private float ActiveIncomeMultiplier;
    [SerializeField]
    private float UpgradeCostMultiplier;
    [SerializeField]
    private ImmutableFloatVariable ActiveIncome;
    public override bool IsAvailable(float money)
    {
        return (money >= NextCost &&
            ActiveIncome.RuntimeValue * ActiveIncomeMultiplier <= UpgradeLimit);
    }

    private void OnEnable()
    {
        ActiveIncome.Reset();
        Reset();
    }

    public override void Reset()
    {
        UpdateNextCost();
    }


    public override void UpdateValue(ImmutableFloatVariable money)
    {
        money.RuntimeValue -= NextCost;
        ActiveIncome.RuntimeValue *= ActiveIncomeMultiplier;
        UpdateNextCost();
    }

    private void UpdateNextCost()
    {
        NextCost = ActiveIncome.RuntimeValue * ActiveIncomeMultiplier * UpgradeCostMultiplier;
    }
}

