﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu]
public class MultiplyingBonus : Bonus
{
    private void OnEnable()
    {
        Amount = PlayerPrefs.GetInt(name, 0);
    }

    public float Multiplier;
    public override void StartBonus()
    {
        GameplayValue.RuntimeValue *= Multiplier;
    }

    public override void EndBonus()
    {
        GameplayValue.RuntimeValue /= Multiplier;
    }
}

