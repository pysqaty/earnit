﻿using UnityEngine;

public abstract class Bonus : ScriptableObject
{
    public int Amount;
    public float RealMoneyCost;
    protected ImmutableFloatVariable GameplayValue;
    public float Duration;
    public bool IsRunning = false;
    public abstract void StartBonus();
    public abstract void EndBonus();
}
