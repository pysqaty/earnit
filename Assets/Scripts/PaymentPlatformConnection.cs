﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaymentPlatformConnection
{
    private static PaymentPlatformConnection _connection = null;
    public static PaymentPlatformConnection Connection
    {
        get
        {
            if(_connection == null)
            {
                _connection = new PaymentPlatformConnection();
            }
            return _connection;
        }
    }

    public float GetAccountFunds()
    {
        return float.MaxValue;
    }
}
